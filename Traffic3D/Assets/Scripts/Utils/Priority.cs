using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Priority of a system, lowest will execute first, highest will execute last
/// </summary>
public enum Priority
{
    HIGH = 3,
    MEDIUM = 2,
    LOW = 1
}
